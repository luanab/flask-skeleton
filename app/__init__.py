from datetime import datetime

__author__ = "Luke Frozz"
__version__ = "1.0"
__maintainer__ = "Luke Frozz"
__email__ = "cunha.ladm@outlook.com"
__status__ = "Development"

from flask import Flask, jsonify
from flask_api import status
from flask_jwt import current_identity, JWT, JWTError
from flask_mail import Mail, Message
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore
from functools import wraps
from sqlalchemy.dialects.postgresql import JSONB, UUID
from sqlalchemy import or_

from passlib.hash import pbkdf2_sha512

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

mail = Mail(app)


def email():
    msg = Message(
        'Works on Production',
        sender='noreply@yoursite.com',
        recipients=['cunha.ladm@gmail.com', 'cunha.ladm@outlook.com'])
    msg.body = "Works on production"
    msg.html = "Works on production"
    mail.send(msg)
    return "Sent"


def envia_email(assunto=None, de='noreply@yoursite.com', para=None, corpo_texto=None, corpo_html=None):
    msg = Message(assunto, sender=de, recipients=para)
    msg.body = corpo_texto
    msg.html = corpo_html
    mail.send(msg)


@app.route("/")
def hello():
    # envia_email(
    #     assunto="HTML TEST",
    #     para=['cunha.ladm@gmail.com', 'cunha.ladm@outlook.com', 'lucas.cunha@stockinvestmoney.com'],
    #     corpo_html=render_template('test.html'),
    #     corpo_texto=render_template('test.txt')
    # )
    return 'API server is working great!'


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers['Last-Modified'] = datetime.now()
    r.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
    r.headers['Pragma'] = 'no-cache'
    r.headers['Expires'] = '-1'
    return r


class Base(db.Model):
    """
    Classe de campos base para uma entidade de banco de dados

    :param __abstract__: indica que a classe serve como base, e não é concreta
    :param id: Chave primária sequencial da tabela
    :param uuid: Chave primária UUID4 da tabela
    :param date_created: Data de criação do registro
    :param date_modified: Data da ultima modificação do registro
    :param active: Indicador de registro

    :type __abstract__: bool
    :type id: int
    :type uuid: UUID4
    :type date_created: datetime.datetime
    :type date_modified: datetime.datetime
    :type active: bool

    note::
        o active indica se o registro foi "apagado" ou não, é através dele que
        é feito a pesquisa no banco para saber quais registros estão disponíveis ou não ao usuário
    """
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    id = db.Column(db.Integer(), primary_key=True, unique=True, autoincrement=True)
    uuid = db.Column(UUID(), primary_key=True, unique=True, default=db.func.uuid_generate_v4())
    date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
                              onupdate=db.func.current_timestamp())
    extra = db.Column(JSONB, default={})

    active = db.Column(db.Boolean, default=True)


class TabelaApoio(Base):
    __tablename__: str = 'tabela_apoio'

    nome = db.Column(db.String, nullable=False)


@app.route('/v1.0/apoio/<string:name>')
def apoios_get(name: str):
    return jsonify(
        items=TabelaApoio.query.with_entities(
            TabelaApoio.extra
        ).filter(
            TabelaApoio.nome == name
        ).first()[0]['items']
    )


from app.mod_auth.models import User, Profile

user_datastore = SQLAlchemyUserDatastore(db, User, Profile)
security = Security(app, user_datastore, login_form=None)

app.config['ASSETS_DEBUG'] = False


def authenticate(username, password):
    user = User.query.filter(
        or_(
            User.email == username,
            User.nickname == username,
            User.cpf_cnpj == username
        ), User.active
    ).first()
    if pbkdf2_sha512.verify(password, user.password):
        return user


def identity(payload):
    id = payload['identity']
    return User.query.filter(User.id == id).first()


jwt = JWT(app, authenticate, identity)

@jwt.jwt_payload_handler
def jwt_payload_handler(identity):
    iat = datetime.utcnow()
    exp = iat + app.config.get('JWT_EXPIRATION_DELTA')
    nbf = iat + app.config.get('JWT_NOT_BEFORE_DELTA')
    _identity = getattr(identity, 'uuid') or identity['uuid']
    return {'exp': exp, 'iat': iat, 'nbf': nbf, 'identity': _identity}


def roles_accepted(roles: list):
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            permissoes = current_identity.roles
            if len(list(filter(lambda p: p.name in roles, permissoes))) > 0:
                return fn(*args, **kwargs)
            else:
                return JWTError(description="Can't access without Profile", error="Forbidden", status_code=403)
        return decorated_view

    return wrapper


@jwt.auth_response_handler
def customized_response_handler(access_token, identity: User):
    return jsonify({
        'access_token': access_token.decode('utf-8'),
        'user': {
            'id': identity.id,
            'name': identity.extra['name'],
            'email': identity.email,
            'nickname': identity.nickname,
            'phone': identity.phone,
            'cpf_cnpj': identity.cpf_cnpj,
            'profiles': list(map(lambda p: {
                'route': p.extra['route'],
                'name': p.name,
                'icon': p.extra['icon']
            }, identity.roles)),
            'reset_token': identity.token == '0'
        }
    })


from app.mod_auth.controllers_v1 import mod_auth_v1
from app.mod_order.controllers_v1 import mod_order_v1

app.register_blueprint(mod_auth_v1)
app.register_blueprint(mod_order_v1)
