from app import Base, db
from sqlalchemy.dialects.postgresql import ARRAY, JSONB
from flask_security import UserMixin, RoleMixin

roles_users = db.Table('user_profile',
                       db.Column('id', db.Integer(), primary_key=True),
                       db.Column('user_id', db.Integer(), db.ForeignKey('auth_user.id')),
                       db.Column('profile_id', db.Integer(), db.ForeignKey('auth_profile.id')),
                       db.Column('extra', JSONB(), default={}))


class Profile(Base, RoleMixin):
    __tablename__ = 'auth_profile'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(Base, UserMixin):
    __tablename__ = 'auth_user'

    email = db.Column(db.String, unique=True, nullable=False)
    nickname = db.Column(db.String, unique=True)
    password = db.Column(db.String)
    cpf_cnpj = db.Column(db.String(14), unique=True, nullable=False)
    phone = db.Column(db.String, unique=True, nullable=False)
    token = db.Column(db.String, nullable=False)
    termos = db.Column(db.Boolean, default=False)
    roles = db.relationship('Profile', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
