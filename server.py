#!venv/bin/python

__author__ = "Luke Frozz"
__version__ = "1.0"
__maintainer__ = "Luke Frozz"
__email__ = "cunha.ladm@outlook.com"
__status__ = "Development"

from app import app, db
from flask_script import Manager, Server
from flask_migrate import Migrate, MigrateCommand

migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)
manager.add_command('runserver', Server(host="0.0.0.0", port=5010))


if __name__ == '__main__':
    manager.run()
